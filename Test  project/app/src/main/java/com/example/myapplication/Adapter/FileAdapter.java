package com.example.myapplication.Adapter;

import android.content.Context;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.myapplication.FileListActivity;
import com.example.myapplication.Utils.FileInfo;
import com.example.myapplication.R;


import java.util.List;

public class FileAdapter extends BaseAdapter {
    List<FileInfo> list;
    LayoutInflater inflater;
    boolean isShowCheckBox=false;
    ViewHolder holder=null;


    public FileAdapter(Context context,SparseBooleanArray stateCheckedMap) {
        this.inflater = LayoutInflater.from(context);
        FileListActivity.stateCheckedMap=stateCheckedMap;
    }
    @Override
    public int getCount() {
        return (list == null) ? 0 : list.size();
    }

    public void setList(List<FileInfo> list) {
        this.list = list;
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View converView, ViewGroup parent) {
        if (converView == null) {
            holder = new ViewHolder();
            converView = inflater.inflate(R.layout.layout_fileitem, null);
            holder.icon = (ImageView) converView.findViewById(R.id.icon);
            holder.play = (ImageView) converView.findViewById(R.id.play);
            holder.name = (TextView) converView.findViewById(R.id.name);
            holder.size = (TextView) converView.findViewById(R.id.desc);
            holder.path = (TextView) converView.findViewById(R.id.path);
            holder.checkBox = (CheckBox) converView.findViewById(R.id.chb_select_way_point);
            converView.setTag(holder);
        } else {
            holder = (ViewHolder) converView.getTag();
        }

        FileInfo item = list.get(position);
        holder.icon.setImageResource(item.getIcon());
        holder.name.setText(item.getName());
        holder.size.setText(item.getSize() + " " + item.getTime());
        showAndHideCheckBox();//控制checkbox框的显示与隐藏
        //checkbox点击事件
        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FileListActivity.stateCheckedMap.put(position,!FileListActivity.stateCheckedMap.get(position));
            }
        });

        holder.checkBox.setChecked(FileListActivity.stateCheckedMap.get(position));

        return converView;
    }
    public void setShowCheckBox(boolean showCheckBox){
        isShowCheckBox=showCheckBox;
    }

    public boolean getShowCheckBox(){
        return isShowCheckBox;
    }

    //设置checkbox选择框是否可见
    public void showAndHideCheckBox() {
        if (isShowCheckBox) {
            holder.checkBox.setVisibility(View.VISIBLE);
        } else {
            holder.checkBox.setVisibility(View.GONE);
        }
    }

    public boolean isShowCheckBox() {
        return isShowCheckBox;
    }

    public class ViewHolder {
            ImageView icon;
            ImageView play;
            TextView name;
            TextView size;
            TextView path;
            public CheckBox checkBox;
    }

}
