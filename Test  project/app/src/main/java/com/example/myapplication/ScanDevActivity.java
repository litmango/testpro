package com.example.myapplication;

import androidx.annotation.UiThread;
import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothClass;
import android.content.Context;
import android.location.Address;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.system.Os;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import com.example.myapplication.Adapter.ScanAdapter;
import com.example.myapplication.Utils.*;
public class ScanDevActivity extends AppCompatActivity {

    private MulticastSocket socket = null;
    private final String BROADCAST_IP = "224.0.0.1";
    private final int BROADCAST_PORT = 27189;
    private final int ANDROID_DEVICE = 1;
    private final int PC_DEVICE = 0;
    private int i;

    private final String brand = Build.BRAND;
    private final String model = Build.MODEL;

    private String uuid;
    private List<DeviceInfo> list = null ;//存放接收到的信息
    ScanAdapter adapter;
    ListView listView ;
    Button button1;
    TextView deviceInfo;
    Message msg;
    private static final int COMPLETED = 0;
    //UI线程更新视图
    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg){
            if (msg.what ==COMPLETED){
                adapter.notifyDataSetChanged();//刷新視圖
            }
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scandev);

        initView();
        send();//发送本机信息



        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                receive();
            }
        });

    }

    public void initView(){
        button1 = findViewById(R.id.button);
        listView = findViewById(R.id.list_view);
        deviceInfo = findViewById(R.id.device_info);
        deviceInfo.setText("本机设备名称：" + brand);
        uuid = Utils.getUniquePseudoID();
        list = new ArrayList<DeviceInfo>();
        adapter = new ScanAdapter(this);
        msg = new Message();
        listView.setAdapter(adapter);
        //组播
        try {
            socket = new MulticastSocket(BROADCAST_PORT);
            socket.joinGroup(InetAddress.getByName(BROADCAST_IP));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * 发送本机信息
     */
    public void send() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    //设置30秒超时
                    //DatagramSocket socket = new DatagramSocket();
                    //socket.setSoTimeout(30000);
                    //创建UDP数据包
                    while (true){
                        String info =uuid + " " + brand + " " + ANDROID_DEVICE + " " + model;
                        Log.i("发送内容",info);
                        byte[] data = info.getBytes();
                        DatagramPacket packet = new DatagramPacket(data, data.length, InetAddress.getByName(BROADCAST_IP), BROADCAST_PORT);
                        socket.send(packet);
                        //Log.i("发送中",".......");

                        Thread.sleep(1000);
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }).start();

    }

    /**
     * 接收设备信息
     */
    public void receive() {
        new Thread(() -> {
            try {
                while (true){
                    //Log.i("接收到的内容","接收中");
                    byte[] ackBuf = new byte[1024];
                    DatagramPacket ackPacket = new DatagramPacket(ackBuf, ackBuf.length, InetAddress.getByName(BROADCAST_IP), BROADCAST_PORT);
                    socket.receive(ackPacket);
                    //屏蔽本机信息
                    String ip = ackPacket.getAddress().getHostAddress();
                    String localip = getIPAddress();
                    if (ip.equals(localip)) {
                        //Log.i("屏蔽","本机");
                        continue;
                    }
                    String data = new String(ackPacket.getData(), 0, ackPacket.getLength());
                    String[] words = data.split(" ");//uuid  品牌  pc/android  型号
                    String receive_uuid = words[0];
                    String receive_brand = words[1];
                    /**
                     * 添加数据至list列表
                     */
                    DeviceInfo item = new DeviceInfo();
                    //判断list是否为空
                    if (list.size() == 0){
                        // Log.i("list","weikong");
                        item.setIcon(R.drawable.pcicon);
                        item.setName(words[1]);
                        item.setUuid(receive_uuid);
                        list.add(item);
                        // Log.i("接收到的内容",item.getUuid());
                        adapter.setList(list);
                        //Log.i("list",list.get(0).getName());
                        //adapter.notifyDataSetChanged();//刷新視圖
                        msg.what = COMPLETED;
                        handler.sendMessage(msg);
                        continue;
                    }
                    for (i=0; i < list.size(); i++){
                        if(receive_uuid.equals(list.get(i).getUuid())){//判断uuid是否已存在
                            // Log.i("list","已存在");
                            break; }
                    }
                    if (i >= list.size()){
                        //添加内容至list；
                        item.setIcon(R.drawable.pcicon);
                        item.setName(words[1]);
                        item.setUuid(receive_uuid);
                        list.add(item);
                        Log.i("接收到新内容",data);
                        Thread.sleep(2000);
                    }
                    //adapter.notifyDataSetChanged();//刷新視圖
                    msg.what = COMPLETED;
                    handler.sendMessage(msg);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
    }



    /**
     * 获得IP地址，分为两种情况，一是wifi下，二是移动网络下，得到的ip地址是不一样的
     * @return
     */
    public String getIPAddress() {
        Context context = ScanDevActivity.this;
        NetworkInfo info = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (info != null && info.isConnected()) {
            if (info.getType() == ConnectivityManager.TYPE_MOBILE) {//当前使用2G/3G/4G网络
                try {
                    //Enumeration<NetworkInterface> en=NetworkInterface.getNetworkInterfaces();
                    for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                        NetworkInterface intf = en.nextElement();
                        for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                            InetAddress inetAddress = enumIpAddr.nextElement();
                            if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                                return inetAddress.getHostAddress();
                            }
                        }
                    }
                } catch (SocketException e) {
                    e.printStackTrace();
                }
            } else if (info.getType() == ConnectivityManager.TYPE_WIFI) {//当前使用无线网络
                WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                //调用方法将int转换为地址字符串
                String ipAddress = intIP2StringIP(wifiInfo.getIpAddress());//得到IPV4地址
                return ipAddress;
            }
        } else {
            //当前无网络连接,请在设置中打开网络
        }
        return null;
    }
    /**
     * 将得到的int类型的IP转换为String类型
     * @param ip
     * @return
     */
    public String intIP2StringIP(int ip) {
        return (ip & 0xFF) + "." +
                ((ip >> 8) & 0xFF) + "." +
                ((ip >> 16) & 0xFF) + "." +
                (ip >> 24 & 0xFF);
    }



}