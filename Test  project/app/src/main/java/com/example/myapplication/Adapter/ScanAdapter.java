package com.example.myapplication.Adapter;

import android.bluetooth.BluetoothClass;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.Utils.DeviceInfo;
import com.example.myapplication.R;

import com.example.myapplication.Utils.DeviceInfo;

import java.util.List;

public class ScanAdapter extends BaseAdapter {
    List<DeviceInfo> list;
    LayoutInflater inflater;
    ViewHolder holder=null;


    public ScanAdapter(Context context){
        this.inflater = LayoutInflater.from(context);
    }
    @Override
    public Object getItem(int i) {
        return null;
    }

    public void setList(List<DeviceInfo> list) {
        this.list = list;
    }

    @Override
    public int getCount() {
        if (list == null) {
            return 0;
        } else {
            return list.size();
        }
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View converView, ViewGroup viewGroup) {
        if (converView == null){
            holder = new ViewHolder();
            converView = inflater.inflate(R.layout.layout_devitem,null);//inflater:解析器
            holder.icon = (ImageView) converView.findViewById(R.id.icon);
            holder.device_name = (TextView) converView.findViewById(R.id.device_name);
            converView.setTag(holder);
        }else {
            holder = (ViewHolder) converView.getTag();
        }

        DeviceInfo item = list.get(position);
        holder.icon.setImageResource(item.getIcon());
        holder.device_name.setText(item.getName());
        return converView;
    }

    public class ViewHolder {
        ImageView icon;
        TextView device_name;
    }

}
