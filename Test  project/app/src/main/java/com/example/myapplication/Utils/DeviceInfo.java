package com.example.myapplication.Utils;

public class DeviceInfo {
    private int icon;
    private String name;
    private String uuid;

    public int getIcon(){
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getName(){
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
