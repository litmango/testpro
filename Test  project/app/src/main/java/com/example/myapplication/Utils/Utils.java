package com.example.myapplication.Utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.Toast;

import androidx.core.content.FileProvider;

import com.example.myapplication.Adapter.FileAdapter;
import com.example.myapplication.FileListActivity;
import com.example.myapplication.R;

import java.io.File;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * 工具类
 */
public class Utils {
    List<FileInfo> list;
    /**
     * 获取SDcard根路径
     * @return
     */
    public static String getSDCardPath(){
        return Environment.getExternalStorageDirectory().getAbsolutePath();
    }

    /**
     * 通过传入的路径,返回该路径下的所有的文件和文件夹列表
     * List<FileInfo>:是返回類型
     */
    public static List<FileInfo> getListData(String path,Context context) {
        Context view = context;
        List<FileInfo> list = new ArrayList<FileInfo>();
        //FileAdapter.ViewHolder holder = (FileAdapter.ViewHolder) view.getTag();


        File pfile = new File(path);// 文件对象
        File[] files = null;// 声明了一个文件对象数组
        if (pfile.exists()) {// 判断路径是否存在

            files=pfile.listFiles();// 该文件对象下所属的所有文件和文件夹列表
        }

        if (files != null && files.length > 0) {// 非空验证
            for (File file : files) {// foreach循环遍历
                FileInfo item = new FileInfo();
                if (file.isDirectory()// 文件夹
                        && file.canRead()//是否可读
                ) {
                    item.setType(FileListActivity.T_DIR);
                    //item.type = FileListActivity.T_DIR;
                    item.setIcon(R.drawable.folder);
                    //item.icon= R.drawable.folder;//图标
                    item.setSize("");
                    //item.size="";//大小
                }
                else if(file.isFile()){// 文件
                    //type=FileListActivity.T_FILE;
                    item.setType(FileListActivity.T_FILE);
                    //擴展名
                    String ext = getFileEXT(file.getName());
                    // 文件的图标
                    //icon=getDrawableIcon(ext);// 根据扩展名获取图标
                    item.setIcon(getDrawableIcon(ext));
                    // 文件的大小
                    item.setSize(getSize(file.length()));


                }else{// 其它
                    item.setIcon(R.drawable.mul_file);
                    //icon=R.drawable.mul_file;
                }

                //item.checkbox=(Boolean) holder.checkBox.isChecked();//给遍历的每一个文件/文件夹 设置checkbox值

                item.setName(file.getName());// 名称
                item.setPath(file.getPath());// 路径

                // 最后修改时间
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                Date date = new Date(file.lastModified());
                String time = sdf.format(date);
                item.setTime(time);
                //item.time=time;
                list.add(item);
            }
            files = null;
        }

        return list;
    }



    ///*截取文件擴展名，若有的話

    public static String getFileEXT(String filename){
        if (filename.contains(".")){

            int dot = filename.lastIndexOf(".");
            String ext = filename.substring(dot+1);
            return ext;
        }else{
            return "";
        }
    }
    /**
     * 获得与扩展名对应的图标资源id
     *
     * @param end 扩展名
     *
     * @return
     */
    public static int getDrawableIcon(String end) {
        int id = 0;
        if (end.equals("asf")) {
            id = R.drawable.asf;
        } else if (end.equals("avi")) {
            id = R.drawable.avi;
        } else if (end.equals("bmp")) {
            id = R.drawable.bmp;
        } else if (end.equals("doc")) {
            id = R.drawable.doc;
        } else if (end.equals("gif")) {
            id = R.drawable.gif;
        } else if (end.equals("html")) {
            id = R.drawable.html;
        } else if (end.equals("apk")) {
            id = R.drawable.iapk;
        } else if (end.equals("ico")) {
            id = R.drawable.ico;
        } else if (end.equals("jpg")) {
            id = R.drawable.jpg;
        } else if (end.equals("log")) {
            id = R.drawable.log;
        } else if (end.equals("mov")) {
            id = R.drawable.mov;
        } else if (end.equals("mp3")) {
            id = R.drawable.mp3;
        } else if (end.equals("mp4")) {
            id = R.drawable.mp4;
        } else if (end.equals("mpeg")) {
            id = R.drawable.mpeg;
        } else if (end.equals("pdf")) {
            id = R.drawable.pdf;
        } else if (end.equals("png")) {
            id = R.drawable.png;
        } else if (end.equals("ppt")) {
            id = R.drawable.ppt;
        } else if (end.equals("rar")) {
            id = R.drawable.rar;
        } else if (end.equals("txt") || end.equals("dat") || end.equals("ini")
                || end.equals("java")) {
            id = R.drawable.txt;
        } else if (end.equals("vob")) {
            id = R.drawable.vob;
        } else if (end.equals("wav")) {
            id = R.drawable.wav;
        } else if (end.equals("wma")) {
            id = R.drawable.wma;
        } else if (end.equals("wmv")) {
            id = R.drawable.wmv;
        } else if (end.equals("xls")) {
            id = R.drawable.xls;
        } else if (end.equals("xml")) {
            id = R.drawable.xml;
        } else if (end.equals("zip")) {
            id = R.drawable.zip;
        } else if (end.equals("3gp")|| end.equals("flv")) {
            id = R.drawable.file_video;
        } else if (end.equals("amr")) {
            id = R.drawable.file_audio;
        } else {
            id = R.drawable.default_fileicon;
        }
        return id;
    }

    /**
     * 打开文件
     * @param context
     * @param aFile
     */
    public static void openFile(Context context,File aFile) {
        // 实例化意图

        Intent intent = new Intent();
        /*添加动作(干什么?)
         *setAction---使用隐式意图不仅可以启动自己程序中的Activity，还可以启动其他程序中的Activity，使得程序之间可以共享某种功能
         */
        intent.setAction(intent.ACTION_VIEW);
        //取得文件名
        String fileName = aFile.getName();
        String end = getFileEXT(fileName).toLowerCase();
        //Log.i("end=",end);
        if (aFile.exists()) {
            Uri uri= FileProvider.getUriForFile(context,"com.example.myapplication.fileprovider",aFile);
            // 根据不同的文件类型来打开文件
            if (checkEndsInArray(end, new String[]{"png","gif","jpg","bmp"})) {
                // 图
                intent.setDataAndType(uri,"image/*");//MIME TYPE的缩写为(Multipurpose Internet Mail Extensions)代表互联网媒体类型，网上有对应表格
            } else if (checkEndsInArray(end, new String[]{"apk"})) {
                // apk
                intent.setDataAndType(uri, "application/vnd.android.package-archive");
            } else if (checkEndsInArray(end, new String[]{"mp3","amr","ogg","mid","wav"})) {
                // audio
                intent.setDataAndType(uri, "audio/*");
            } else if (checkEndsInArray(end, new String[]{"mp4","3gp","mpeg","mov","flv"})) {
                // video
                intent.setDataAndType(uri, "video/*");
            } else if (checkEndsInArray(end, new String[]{"txt","dat","ini","log","java","xml","html"})) {
                // text
                intent.setDataAndType(uri, "text/*");
            } else if (checkEndsInArray(end, new String[]{"doc","docx","pdf"})) {
                // word
                intent.setDataAndType(uri, "application/msword");

            } else if (checkEndsInArray(end, new String[]{"xls","xlsx"})) {
                // excel
                intent.setDataAndType(uri, "application/vnd.ms-excel");
            } else if (checkEndsInArray(end, new String[]{"ppt","pptx"})) {
                // ppt
                intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
            } else if (checkEndsInArray(end, new String[]{"chm"})) {
                // chm
                intent.setDataAndType(uri, "application/x-chm");
            } else {
                // 其他
                intent.setDataAndType(uri, "application/" + end);
            }
            //context.startActivity(intent);
            //Log.i("spl", "发送="+end);
            try {// 发送意图

                //解决intent通过FileProvider读取图片资源黑屏的问题
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                Log.i("图",end);
            } catch (Exception e) {
                Toast.makeText(context, "没有找到适合打开此文件的应用", Toast.LENGTH_SHORT).show();

            }

        }
    }
    /**
     * 检查扩展名end 是否在ends数组中
     * @param end
     * @param ends
     * @return
     */
    public static boolean checkEndsInArray(String end, String[] ends) {
        for (String aEnd : ends) {
            if (end.equals(aEnd)) {
                return true;
            }
        }
        return false;
    }
    /**
     * 文件单位格式轉換"B,KB,MB,GB"
     */
    public static String getSize(float length) {

        long kb = 1024;
        long mb = 1024*kb;
        long gb = 1024*mb;
        if(length<kb){
            return String.format("%dB", (int)length);
        }else if(length<mb){
            return String.format("%.2fKB", length/kb);
        }else if(length<gb){
            return String.format("%.2fMB", length/mb);
        }else {
            return String.format("%.2fGB", length/gb);
        }
    }

    /**
     * 获得独一无二的 Pseudo ID
     */
    public static String getUniquePseudoID() {
        String serial = null;
        String m_szDevIDShort = "35" +
                Build.BOARD.length() % 10 + Build.BRAND.length() % 10 +

                Build.CPU_ABI.length() % 10 + Build.DEVICE.length() % 10 +

                Build.DISPLAY.length() % 10 + Build.HOST.length() % 10 +

                Build.ID.length() % 10 + Build.MANUFACTURER.length() % 10 +

                Build.MODEL.length() % 10 + Build.PRODUCT.length() % 10 +

                Build.TAGS.length() % 10 + Build.TYPE.length() % 10 +

                Build.USER.length() % 10; //13 位

        try {
            serial = Build.class.getField("SERIAL").get(null).toString();
            //API>=9 使用serial号
            return new UUID(m_szDevIDShort.hashCode(), serial.hashCode()).toString();
        } catch (Exception exception) {
            //serial需要一个初始化
            serial = "serial";
        }
        //使用硬件信息拼凑出来的15位号码
        return new UUID(m_szDevIDShort.hashCode(), serial.hashCode()).toString();
    }
}
