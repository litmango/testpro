package com.example.myapplication;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import com.example.myapplication.Adapter.FileAdapter;
import com.example.myapplication.Utils.FileInfo;
import com.example.myapplication.Utils.Utils;
import com.google.android.material.resources.CancelableFontCallback;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileListActivity extends AppCompatActivity{

    private ListView listView;
    private List<FileInfo> list;
    FileAdapter adapter;
    TextView tvPath;
    String currentPath;
    String parentPath;
    ImageView back;
    LinearLayout editBar;//控制bar的显示与隐藏
    Button cancel;
    Button selectALL;
    List<FileInfo> mCheckedData;//选中数据放入
   //将选中数据放入里面
    public static SparseBooleanArray stateCheckedMap = new SparseBooleanArray();

    //用来存放CheckBox的选中状态

    final String ROOT= Utils.getSDCardPath();//獲取SDcard根目錄

    public static final int T_DIR = 0;
    public static final int T_FILE = 1;

    // 要申请的权限
    private String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CALL_PHONE,
            Manifest.permission.CAMERA,Manifest.permission.ACCESS_COARSE_LOCATION};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filelist);
        // 版本判断。当手机系统大于 16 时，才有必要去判断权限是否获取
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // 检查该权限是否已经获取
            int i = ContextCompat.checkSelfPermission(getApplicationContext(), permissions[0]);
            int l = ContextCompat.checkSelfPermission(getApplicationContext(), permissions[1]);
            int m = ContextCompat.checkSelfPermission(getApplicationContext(), permissions[2]);
            int n = ContextCompat.checkSelfPermission(getApplicationContext(), permissions[3]);
            // 权限是否已经 授权 GRANTED---授权  DINIED---拒绝
            if (i != PackageManager.PERMISSION_GRANTED || l != PackageManager.PERMISSION_GRANTED || m != PackageManager.PERMISSION_GRANTED ||
                    n != PackageManager.PERMISSION_GRANTED) {
                // 如果没有授予该权限，就去提示用户请求
                startRequestPermission();
            }
        }

        initView();//初始化
        updateData(ROOT);//获取数据
        setOnItemClickListener();
        setOnItemLongClickListener();
        backOnClickListener();
        cancelOnClickListener();
        selectALLOnClickListener();
    }


    /**
     * 点击item事件
     */
    public void setOnItemClickListener(){
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                FileInfo item = (FileInfo) adapterView.getItemAtPosition(i);
                if (item.getType() == T_DIR) {
                    //进入文件夾
                    updateData(item.getPath());
                } else {
                    //打开文件
                    Utils.openFile(FileListActivity.this, new File(item.getPath()));
                }
            }
        });
    }


    /**
     * item长按点击事件
     */

    public void setOnItemLongClickListener(){
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                FileInfo item = (FileInfo) adapterView.getItemAtPosition(i);
                editBar.setVisibility(View.VISIBLE);//editbar可见
                adapter.setShowCheckBox(true);//设置checkbox框可见
                //updateCheckBoxStatus(view, i);
                adapter.notifyDataSetChanged();
                return true;
            }
        });
    }


    /*
     **返回
     */
    public void backOnClickListener(){
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    /**
     * 取消
     */
    public void cancelOnClickListener(){
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel();
            }
        });
    }

    /**
     * 全选
     **/
    public void selectALLOnClickListener(){
        selectALL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setStateCheckedMap(true);
                adapter.notifyDataSetChanged();
            }
        });
    }

    /**
     * 取消选择
     */
    public void cancel(){
        setStateCheckedMap(false);//将checkbox所有状态置位未选中
        editBar.setVisibility(View.GONE);//隐藏下方选择栏
        adapter.setShowCheckBox(false);//隐藏checkbox框框
        adapter.notifyDataSetChanged();
    }

    /**
     * 设置所有checkbox的选中状态（取消和全选的时候用到）
     * @param isSelectedAll
     */
    private void setStateCheckedMap(boolean isSelectedAll) {

        for (int i = 0; i < list.size(); i++) {
            stateCheckedMap.put(i, isSelectedAll);
            listView.setItemChecked(i, isSelectedAll);//设置listview点击position位置的item项
        }
    }

    /**
     *  更新checkbox 相关状态
     */
    public void updateCheckBoxStatus(View view,int position){
        //Log.i("通过=",currentPath);
        FileAdapter.ViewHolder holder = (FileAdapter.ViewHolder) view.getTag();
        //holder.checkBox.toggle();
        listView.setItemChecked(position, holder.checkBox.isChecked());//选中项设置高亮
        stateCheckedMap.put(position,holder.checkBox.isChecked());//isChecked()区分CheckBox是否被选中，isChecked有两种返回值：0,1
    /*
        if (holder.checkBox.isChecked()){
            mCheckedData.add(list.get(position));//CheckBox选中时，把这一项的数据加到选中数据列表
        }else {
            mCheckedData.remove(list.get(position));
        }*/
        adapter.notifyDataSetChanged();
    }

    /**
     * 更新listview布局显示
     * @param path
     */
    public void updateData (String path){
        currentPath = path;//路徑
        File file = new File(path);
        parentPath = file.getParent();//上一級路徑
        list = Utils.getListData(currentPath,this);//數據
        adapter.setList(list);
        adapter.notifyDataSetChanged();//刷新視圖
        tvPath.setText(currentPath);
    }


    /**
     * 返回按钮
     */
    @Override
    public void onBackPressed () {
        if (currentPath.equals(ROOT)) {
            // 退出流程
            setStateCheckedMap(false);
            finish();
        } else {
            //打开上目录
            updateData(parentPath);
        }
    }


    /**
     * 初始化
     */

    public void initView () {
        tvPath = (TextView) findViewById(R.id.path);
        listView = (ListView) findViewById(R.id.list_view);
        back = (ImageView) findViewById(R.id.back);
        cancel = (Button) findViewById(R.id.cancel);
        selectALL = (Button) findViewById(R.id.select_all);

        //checkbox初始化------editerbar
        editBar = findViewById(R.id.edit_bar);
        //设置listview可多选
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        adapter = new FileAdapter(this,stateCheckedMap);
        listView.setAdapter(adapter);
        //listView.setOnClickListener(this);

    }


    /**
     * 开始提交请求权限
     */
    private void startRequestPermission () {
        ActivityCompat.requestPermissions(this, permissions, 321);
    }

    /**
     * 用户权限 申请 的回调方法
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult ( int requestCode, @NonNull String[] permissions,
    @NonNull int[] grantResults){
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 321) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    //如果没有获取权限，那么可以提示用户去设置界面--->应用权限开启权限
                } else {
                    //获取权限成功提示，可以不要
                    Toast toast = Toast.makeText(this, "获取权限成功", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        }
    }


}